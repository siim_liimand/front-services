import { GetPageData } from './interfaces';

const host = typeof window === 'undefined' ? process.env.HOST : window.location.protocol + '//' + window.location.host;

const GET_PAGE_DATA = '/api/get-page-data';

const METHOD_GET = 'GET';

const HEADERS_JSON = { 'Content-Type': 'application/json' };

export const getPageData: GetPageData = (fetch, siteName, path, query, resolve, reject) => {
  const url = host + GET_PAGE_DATA + `?site=${siteName}&path=${path}&query=${query}`;
  fetch(url, {
    method: METHOD_GET,
    headers: HEADERS_JSON,
  })
    .then((res) => res.json())
    .then((json) => resolve(json))
    .catch((err) => reject(err));
};
