export interface FetchInit {
  body?: any | undefined;
  headers?: any | undefined;
  method?: any | undefined;
}
export type Fetch = (input: string, init?: FetchInit) => Promise<any>;

export type Resolve<T> = (data: T) => void;
export type Reject = (err: string) => void;
export type Query = URLSearchParams;
export interface Seo {
  title: string;
  description: string;
  image: string;
}
export type Countries = {
  code: string;
  languages: {
    code: string;
    title: string;
  }[];
  title: string;
}[];
export type UID = string;
export interface Page {
  id: UID;
  parentId: UID | undefined;
  // Only default seo is stored dynamically to the file
  seo?: Seo;
  title: string;
  url: string;
}
export type Pages = Record<UID, Page>;
export interface MenuItem {
  children: MenuItem[];
  icon?: string;
  pageId: UID;
}
export interface Colors {
  primary: string;
  secondary: string;
}
export interface Typography {}
export interface Theme {
  colors: Colors;
  typography: Typography;
}
export interface InitialPageData {
  countryCode: string;
  languageCode: string;
  // Default countries are stored dynamically to the file
  countries?: Countries;
  // Default footer menu is stored dynamically to the file
  footerMenu?: MenuItem[];
  // Default mega menu is stored dynamically to the file
  megaMenu?: MenuItem[];
  // Default pages are stored dynamically to the file
  pages?: Page[];
  // Default theme is stored dynamically to the file
  theme?: Theme;
  // Default top menu is stored dynamically to the file
  topMenu?: MenuItem[];
  // Dynamically store translations to the file
  // translations: Record<string, string>;
}
export type BreadCrumb = UID[];
export interface PageData extends Record<string, unknown> {
  breadcrumb: BreadCrumb;
  // Includes only important data
  contentData: Record<string, unknown>;
  // Default seo is stored dynamically to the pages file
  seo?: Seo;
  // Will be filled on first load or on language change
  initialPageData?: InitialPageData;
  title: string;
}
export type GetPageData = (
  fetch: Fetch,
  siteName: string,
  path: string,
  query: Query,
  resolve: Resolve<PageData>,
  reject: Reject,
) => void;
