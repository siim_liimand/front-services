export interface FetchInit {
    body?: any | undefined;
    headers?: any | undefined;
    method?: any | undefined;
}
export declare type Fetch = (input: string, init?: FetchInit) => Promise<any>;
export declare type Resolve<T> = (data: T) => void;
export declare type Reject = (err: string) => void;
export declare type Query = URLSearchParams;
export interface Seo {
    title: string;
    description: string;
    image: string;
}
export declare type Countries = {
    code: string;
    languages: {
        code: string;
        title: string;
    }[];
    title: string;
}[];
export declare type UID = string;
export interface Page {
    id: UID;
    parentId: UID | undefined;
    seo?: Seo;
    title: string;
    url: string;
}
export declare type Pages = Record<UID, Page>;
export interface MenuItem {
    children: MenuItem[];
    icon?: string;
    pageId: UID;
}
export interface Colors {
    primary: string;
    secondary: string;
}
export interface Typography {
}
export interface Theme {
    colors: Colors;
    typography: Typography;
}
export interface InitialPageData {
    countryCode: string;
    languageCode: string;
    countries?: Countries;
    footerMenu?: MenuItem[];
    megaMenu?: MenuItem[];
    pages?: Page[];
    theme?: Theme;
    topMenu?: MenuItem[];
}
export declare type BreadCrumb = UID[];
export interface PageData extends Record<string, unknown> {
    breadcrumb: BreadCrumb;
    contentData: Record<string, unknown>;
    seo?: Seo;
    initialPageData?: InitialPageData;
    title: string;
}
export declare type GetPageData = (fetch: Fetch, siteName: string, path: string, query: Query, resolve: Resolve<PageData>, reject: Reject) => void;
//# sourceMappingURL=interfaces.d.ts.map